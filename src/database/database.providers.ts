import { Sequelize } from 'sequelize-typescript'
import { Mission } from '../missions/model/Mission'

export const databaseProviders = [
	{
		provide: 'SequelizeToken',
		useFactory: async () => {
			const sequelize = new Sequelize( {
				operatorsAliases: false,
				dialect: 'mysql',
				host: 'localhost',
				port: 3306,
				username: 'root',
				password: '',
				database: 'm16'
			} )
			sequelize.addModels( [ Mission ] )
			await sequelize.sync()
			return sequelize
		}
	}
]
