import { Column, Model, PrimaryKey, Sequelize, Table } from 'sequelize-typescript'

@Table({ tableName: 'missions' })
export class Mission extends Model<Mission> {
	@PrimaryKey
	@Column
	id: number
	@Column agent_name: string
	@Column country: string
	@Column address: string
	@Column datetime: string
}
