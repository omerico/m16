import { Module } from '@nestjs/common'
import { MissionsService } from './missions.service'
import { missionsProviders } from './missions.providers'
import { DatabaseModule } from '../database/database.module'
import { MissionsController } from './missions.controller'
import { MapsModule } from '../maps/maps.module'

@Module( {
	imports: [ DatabaseModule, MapsModule ],
	controllers: [ MissionsController ],
	providers: [ MissionsService, ...missionsProviders ]
} )
export class MissionsModule {
}
