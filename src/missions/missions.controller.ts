import { Body, Controller, Get, HttpCode, Post, Res } from '@nestjs/common'
import { MissionsService } from './missions.service'
import { Mission } from './model/Mission'
import { ClosestFarthestMissions } from './model/ClosestFarthestMissions'
import Country from './model/Country'

@Controller( 'missions' )
export class MissionsController {
	constructor( private readonly missionsService: MissionsService ) {
	}

	@Get( '/' )
	async findAll(): Promise<Mission[]> {
		return await this.missionsService.findAll()
	}

	@Get( '/countries-by-isolation' )
	async findMostIsolatedCountry(): Promise<Country[]> {
		return await this.missionsService.findMostIsolatedCountry()
	}

	@Post( '/find-closest' )
	@HttpCode( 200 )
	async findClosestMission( @Body( 'target-location' ) address: string, @Res() res ): Promise<ClosestFarthestMissions> {
		const results: ClosestFarthestMissions = await this.missionsService.findClosestAndFarthestMissions( address )

		if ( results === null ) {
			res.status( 204 ).send( null )
			return null
		} else {
			res.send( results )
		}
	}
}
