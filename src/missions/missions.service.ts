import { Inject, Injectable } from '@nestjs/common'
import { Mission } from './model/Mission'
import { Sequelize } from 'sequelize-typescript'
import { databaseProviders } from '../database/database.providers'
import { createClient, google, GoogleMapsClient } from '@google/maps'
import { MapsService } from '../maps/maps.service'
import { ClosestFarthestMissions } from './model/ClosestFarthestMissions'
import { ClosestFarthestLocations } from '../maps/model/ClosestFarthestLocations'
import Country from './model/Country'

@Injectable()
export class MissionsService {
	constructor( @Inject( 'MissionsRepository' ) private readonly missions: typeof Mission,
				 private readonly maps: MapsService
	) {
	}

	public async findAll(): Promise<Mission[]> {
		return await this.missions.findAll<Mission>( {
			order: [ [ 'datetime', 'ASC' ] ]
		} )
	}

	public async findMostIsolatedCountry(): Promise<Country[]> {
		const sequelize: Sequelize = await databaseProviders[ 0 ].useFactory()
		const results: any = await sequelize.query( `
          SELECT country, COUNT(country) AS isolated_agents
          FROM (SELECT *
                FROM (
                       SELECT agent_name, COUNT(country) AS countries, country
                       FROM missions
                       GROUP BY agent_name
                     ) t
                WHERE countries = 1) b
          GROUP BY country
          ORDER BY isolated_agents DESC` )

		return results[ 0 ]
	}

	/**
	 * Find among a list of addresses of the missions and given address - the most
	 * extreme locations (closest and farthest)
	 * Then, convert the locations into actual missions using hashmap
	 *
	 * @param address {string}
	 * @return {Promise<ClosestFarthestMissions>}
	 */
	async findClosestAndFarthestMissions( address: string ): Promise<ClosestFarthestMissions> {
		const missions: Mission[] = await this.findAll()
		const missionsMap: Map<string, Mission> = new Map<string, Mission>()
		const targetMissions = new ClosestFarthestMissions()

		// Create new hashmap with address as the key and mission as the value
		for ( const mission of missions ) {
			missionsMap[ mission.address + ', ' + mission.country ] = mission
		}

		const addresses: string[] = Object.keys( missionsMap )
		const locations: ClosestFarthestLocations = await this.maps.findClosestFarthestLocation( address, addresses )

		if ( locations === null ) {
			return null
		}

		targetMissions.closest = missionsMap[ locations.closest ]
		targetMissions.farthest = missionsMap[ locations.farthest ]

		return targetMissions
	}
}
