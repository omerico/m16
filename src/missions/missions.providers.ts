import { Mission } from './model/Mission'

export const missionsProviders = [
	{
		provide: 'MissionsRepository',
		useValue: Mission
	}
]
