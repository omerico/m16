import { Module } from '@nestjs/common'
import { MapsService } from './maps.service'
import { mapsProviders } from './maps.providers'

@Module( {
	imports: [],
	providers: [ MapsService, ...mapsProviders ],
	exports: [ MapsService, ...mapsProviders ]
} )
export class MapsModule {
}
