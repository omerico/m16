import { createClient, GoogleMapsClient } from '@google/maps'
import { Injectable } from '@nestjs/common'
import { ClosestFarthestLocations } from './model/ClosestFarthestLocations'
import DistanceMatrixRequest = google.maps.DistanceMatrixRequest
import DistanceMatrixResponse = google.maps.DistanceMatrixResponse
import DistanceMatrixResponseElement = google.maps.DistanceMatrixResponseElement

@Injectable()
export class MapsService {
	private geoApi: GoogleMapsClient

	/**
	 * Receives an array of addresses and a set of distances and creates
	 * a hashmap with address as the value and the distance as the key
	 *
	 * @param addresses {string[]}
	 * @param elements {DistanceMatrixResponseElement[]}
	 */
	private static createAddressesDistancesMap( addresses: string[], elements: DistanceMatrixResponseElement[] ): Map<number, string> {
		const distances: Map<number, string> = new Map<number, string>()

		let i = 0

		for ( const result of elements ) {
			if ( String( result.status ) === 'OK' ) {
				distances[ result.distance.value ] = addresses[ i ]
			}

			i++
		}

		return distances
	}

	public constructor() {
		this.geoApi = createClient( {
			key: 'AIzaSyCjS22PkttiCPaRxUQtpXe9DNYNAsf37as',
			Promise
		} )
	}

	private async fetchDistances( addresses: string[], address: string ): Promise<DistanceMatrixResponse> {
		const request: DistanceMatrixRequest = {
			destinations: addresses,
			origins: [ address ]
		}

		return new Promise<DistanceMatrixResponse>( ( resolve, reject ) => {
			this.geoApi.distanceMatrix( request, ( error, response ) => {
				if ( error ) {
					reject( error )
				}

				resolve( response.json )
			} )
		} )
	}

	/**
	 * regv
	 *
	 * @param address {string}
	 * @param addresses {string[]}
	 */
	public async findClosestFarthestLocation( address: string, addresses: string[] ): Promise<ClosestFarthestLocations> {
		const results: DistanceMatrixResponse = await this.fetchDistances( addresses, address )
		const distancesMap = MapsService.createAddressesDistancesMap( addresses, results.rows[ 0 ].elements )

		// Get the map keys (distances), convert all to numbers and find min and max
		if ( !Object.keys( distancesMap ).length ) {
			// No results found
			return null
		}

		const distances = Object.keys( distancesMap ).map( Number )
		const maximumDistance = Math.max( ...distances )
		const minimumDistance = Math.min( ...distances )

		const locations: ClosestFarthestLocations = new ClosestFarthestLocations()

		locations.farthest = distancesMap[ maximumDistance ]
		locations.closest = distancesMap[ minimumDistance ]

		return locations
	}
}
