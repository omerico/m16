import { createClient } from '@google/maps'

export const mapsProviders = [ {
	provide: 'GoogleMaps',
	useFactory: async () => {
		return createClient( {
			key: 'YOUR-API-KEY'
		} )
	}
} ]
