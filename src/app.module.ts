import { Module } from '@nestjs/common'
import { MissionsModule } from './missions/missions.module'
import { MapsModule } from './maps/maps.module'

@Module( {
	imports: [ MissionsModule, MapsModule ]
} )
export class AppModule {
}
