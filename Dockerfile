FROM node:latest
RUN apt-get update
RUN apt-get install -y mysql-server

ENV MYSQL_ROOT_PASSWORD=12345
ENV MYSQL_DATABASE=m16
ENV MYSQL_USER=root
ENV MYSQL_PASSWORD=12345

WORKDIR /app
COPY . /app
RUN npm install
CMD node dist/main.js

EXPOSE 3000
