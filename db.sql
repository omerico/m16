CREATE DATABASE m16;
USE m16;

CREATE TABLE missions
(
  id         int(3) PRIMARY KEY AUTO_INCREMENT,
  agent_name varchar(3)   NOT NULL,
  country    varchar(20)  NOT NULL,
  address    varchar(255) NOT NULL,
  datetime   datetime     NOT NULL
);

INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (1, '007', 'Brazil', 'Avenida Vieira Souto 168 Ipanema, Rio de Janeiro', '1995-12-12 09:45:17.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (2, '005', 'Poland', 'Rynek Glowny 12, Krakow', '2011-04-05 17:05:12.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (3, '007', 'Morocco', '27 Derb Lferrane, Marrakech', '2001-01-01 12:00:00.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (4, '005', 'Brazil', 'Rua Roberto Simonsen 122, Sao Paulo', '1986-05-05 08:40:23.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (5, '011', 'Poland', 'Swietego Tomasza 35, Krakow', '1997-09-07 19:12:53.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (6, '003', 'Morocco', 'Rue Al-Aidi Ali Al-Maaroufi, Casablanca', '2012-08-29 10:17:05.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (7, '008', 'Brazil', 'Rua tamoana 418, Tefe', '2005-11-10 13:25:13.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (8, '013', 'Poland', 'Zlota 9, Lublin', '2002-10-17 10:52:19.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (9, '002', 'Morocco', 'Riad Sultan 19, Tangier', '2017-01-01 17:00:00.000000');
INSERT INTO m16.missions (id, agent_name, country, address, datetime) VALUES (10, '009', 'Morocco', 'Atlas Marina Beach, Agadir', '2016-12-01 21:21:21.000000');
